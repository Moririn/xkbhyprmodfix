# xkbhyprmodfix

Replace CapsLock with Hyper_L without keybinds assigned to Hyper clashing with those on Super.

Creates a new xkb keymap, "HyperCaps", identical to the user's current keyboard layout, except with CapsLock replaced with Hyper and Hyper_L moved to Mod3. The symbol file for the current layout and the xkb rules files in `/usr/share/X11/xkb` are cloned to `/usr/local/share/X11/xkb/`, edited, and then symlinked to. The original unmodified files are backed up.

This is a workaround to desktop environments such as KDE overwriting any keyboard setup done with xinit configuration files.

Must be run as root (to grant access to `/usr/share` and `/usr/local/share`).

The new keyboard variant can be accessed by running `setxkbmap -layout <your layout> -variant hypercaps`, or by selecting it in the graphical keyboard settings menu.
